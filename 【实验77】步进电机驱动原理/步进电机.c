/********************************************************************
* 文件名  :  步进电机.c
* 描述    :  用单片机驱动ULN2003去控制步进电机。
			 这个是最简单的一个控制步进电机的方法。		 
* 创建人  ： 东流，2012年2月8日
* 版本号  ： 1.0
* 杜邦线接法：
P1.3用杜邦线连接到J17的D端。
P1.4用杜邦线连接到J17的C端。
P1.5用杜邦线连接到J17的B端。
P1.6用杜邦线连接到J17的A端。
步进电机接到J18的五个端口，其中，步进电机的红线接J18的VCC端。
***********************************************************************/
#include <reg52.h> 
#define uchar unsigned char
#define uint  unsigned int

sbit A1 = P1^3;
sbit A2 = P1^4;
sbit A3 = P1^5;
sbit A4 = P1^6;
 
#define step1 {A1=0;A2=1;A3=1;A4=1;}
#define step2 {A1=0;A2=0;A3=1;A4=1;}
#define step3 {A1=1;A2=0;A3=1;A4=1;}
#define step4 {A1=1;A2=0;A3=0;A4=1;}
#define step5 {A1=1;A2=1;A3=0;A4=1;}
#define step6 {A1=1;A2=1;A3=0;A4=0;}
#define step7 {A1=1;A2=0;A3=1;A4=0;}
#define step8 {A1=0;A2=0;A3=1;A4=0;}

/********************************************************************
* 名称 : Delay_1ms()
* 功能 : 延时子程序，延时时间为 1ms * x
* 输入 : x (延时一毫秒的个数)
* 输出 : 无
***********************************************************************/
void Delay(uint i)
{
	uchar x,j;
	for(j=0;j<i;j++)
	for(x=0;x<=148;x++);	
}

main()
{
	while(1)
 	{ 
		step1;
		Delay(4);
		step2;
		Delay(4);
		step3;
		Delay(4);
		step4;
		Delay(4);
		step5;
		Delay(4);
		step6;
		Delay(4);
		step7;
		Delay(4);
		step8;
		Delay(4);	
  	}
}
