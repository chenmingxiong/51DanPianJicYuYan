/********************************************************************
* 文件名  :  步进电机.c
* 描述    :  用单片机驱动ULN2003去控制步进电机。
			 数码管旋转的角度会在数码管上进行显示。			 
* 创建人  ： 东流，2012年2月8日
* 版本号  ： 1.0
* 杜邦线接法：
P1.3用杜邦线连接到J17的D端。
P1.4用杜邦线连接到J17的C端。
P1.5用杜邦线连接到J17的B端。
P1.6用杜邦线连接到J17的A端。
步进电机接到J18的五个端口，其中，步进电机的红线接J18的VCC端。
***********************************************************************/
#include <reg52.h> 
#define uchar unsigned char
#define uint  unsigned int

uchar  Step = 0;
bit FB_flag = 0;

unsigned char code F_Rotation[8]={0x08,0x18,0x10,0x30,0x20,0x60,0x40,0x48};    //顺时针转表格
unsigned char code B_Rotation[8]={0x48,0x40,0x60,0x20,0x30,0x10,0x18,0x08};    //逆时针转表格

uchar code table[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};
uchar code LED_W[8] = {0xfe,0xfd,0xfb,0xf7,0xef,0xdf,0xbf,0x7f};

/********************************************************************
* 名称 : Delay_1ms()
* 功能 : 延时子程序，延时时间为 1ms * x
* 输入 : x (延时一毫秒的个数)
* 输出 : 无
***********************************************************************/
void Delay(uint i)
{
	uchar x,j;
	for(j=0;j<i;j++)
	for(x=0;x<=148;x++);	
}

main()
{
 	uchar i,count=0;
	uint j;
	uint k = 0;
	while(1)
	{
		for(k=0;k<512;k++)
		{
	  		for(i=0;i<8;i++)      	 //因为有8路的控制时序
	    	{
	     		P1 = F_Rotation[i];  //顺时针转动
	     		Delay(2);        	 //改变这个参数可以调整电机转速
			}
			j=(uint)((360000/512) * k /1000);
			P0 = table[j%10];
			P2 = LED_W[0];
			Delay(1); 
	
			P0 = table[j/10%10];
			P2 = LED_W[1];
			Delay(1); 
		
			P0 = table[j/100%10];
			P2 = LED_W[2];
			Delay(1);
		} 
	}
}
