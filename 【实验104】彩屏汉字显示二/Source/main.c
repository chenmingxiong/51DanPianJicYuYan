/*
================================================================================
Copytight      : Yihetech Co,.Ltd, All rightd reserved.
Store          : 

File Name      : main.c
Description    :  
Date           : 2009-7-19 21:17:45
Version        : V1.0
Author         : LiYong(李勇), yihe_liyong@126.com
Target device  : STC90C516RD+
Compiler       : AVRStudio + GCCAVR
Note           : 转载时务必保留此信息，否则无权以任何形式传播本文件
================================================================================
*/

#include <reg51.h>
#include "MyTypedef.h"
#include "GUI_Basic.h"
#include "LCD.H"			 



    RECT rect  ;
    CIRCLE circle ;
    LINE line;
    FONT font ;
    ELLIPSE ellipse;

void main( void )
{	
  
    LCD_Init( ); 

	GUI_Inital( 0x0000 );
	PutGB3232(0, 0, "好",0x0800,0xF800);
	PutGB3232(32, 0, "好",0x0800,0xFFE0);
	PutGB3232(64, 0, "学",0x0800,0x001F);
	PutGB3232(96, 0, "习",0x0800,0x07E0);
	PutGB3232(0, 32, "天",0x0800,0x07FF);
	PutGB3232(32, 32, "天",0x0800,0x40EE);
	PutGB3232(64, 32, "向",0x0800,0xFAE0);
	PutGB3232(96, 32, "上",0x0800,0xA85F);

	LCD_PutString(26,70,"彩屏汉字",0x0800,0x0c0c);
	LCD_PutString(26,90,"测试代码",0x0800,0xff00);
	while( 1 );
}

/*
================================================================================
-----------------------------------End of file----------------------------------
================================================================================
*/
