/*
================================================================================
Copytight      : Yihetech Co,.Ltd, All rightd reserved.
Store          : 

File Name      : main.c
Description    :  
Date           : 2009-7-19 21:17:45
Version        : V1.0
Author         : LiYong(李勇), yihe_liyong@126.com
Target device  : STC90C516RD+
Compiler       : AVRStudio + GCCAVR
Note           : 转载时务必保留此信息，否则无权以任何形式传播本文件
================================================================================
*/

#include <reg51.h>
#include "MyTypedef.h"
#include "GUI_Basic.h"
#include "LCD.H"			 



    RECT rect  ;
    CIRCLE circle ;
    LINE line;
    FONT font ;
    ELLIPSE ellipse;

void main( void )
{	
  
    LCD_Init( );
	font.Height = 14;
	font.Width = 7;
	font.Color = 0x0800;
	font.BackColor = 0xFF00;

	GUI_Inital( 0x0000 );
	LCD_PutString(26,10,"东流电子",0x0800,0xff00); 
	GUI_DisplayStr(35,30,&font,"HOT-51");
	LCD_PutString(10,50,"单片机开发板",0x0800,0xff00); 
	LCD_PutString(26,70,"彩屏汉字",0x0800,0xff00);
	LCD_PutString(26,90,"测试代码",0x0800,0xff00);
	while( 1 );
}

/*
================================================================================
-----------------------------------End of file----------------------------------
================================================================================
*/
