/********************************************************************
* 文件名  :  步进电机.c
* 描述    :  用单片机驱动ULN2003去控制步进电机。
			 设定步进电机旋转的角度，首先我们设定旋转的初始角度为90度。
			 按S22设定角度加1，按S20设定的角度减1.
			 我们加上了长短键控制，如果长按，会发现角度变化 很快。
			 角度范围是0--360度。
			 设置好角度后，按S19后，步进电机顺时针旋转，按S21，步进电机逆时针旋转。		 
* 创建人  ： 东流，2012年2月8日
* 版本号  ： 1.0
* 杜邦线接法：
P1.3用杜邦线连接到J17的D端。
P1.4用杜邦线连接到J17的C端。
P1.5用杜邦线连接到J17的B端。
P1.6用杜邦线连接到J17的A端。
步进电机接到J18的五个端口，其中，步进电机的红线接J18的VCC端。
***********************************************************************/
#include <reg52.h> 
#define uchar unsigned char
#define uint  unsigned int
 
sbit KEY1 = P3^2;   //步进电机顺时针方向转
sbit KEY2 = P3^3;   //设定角度加一度
sbit KEY3 = P3^4;	//设定角度减一度

uchar  Step = 0;
bit FB_flag = 0;
uint angle = 90;

unsigned char code F_Rotation[8]={0x08,0x18,0x10,0x30,0x20,0x60,0x40,0x48};    //顺时针转表格
unsigned char code B_Rotation[8]={0x48,0x40,0x60,0x20,0x30,0x10,0x18,0x08};    //逆时针转表格

uchar code table[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};
uchar code LED_W[8] = {0xfe,0xfd,0xfb,0xf7,0xef,0xdf,0xbf,0x7f};

/********************************************************************
* 名称 : Delay_1ms()
* 功能 : 延时子程序，延时时间为 1ms * x
* 输入 : x (延时一毫秒的个数)
* 输出 : 无
***********************************************************************/
void Delay(uint i)
{
	uchar x,j;
	for(j=0;j<i;j++)
	for(x=0;x<=148;x++);	
}

void Delay_LED(uchar del)
{
	uchar i;
	for(i=0;i<del;i++)
	{
		P0 = 0x00;
		P2 = 7;
		P0 = table[angle%10];
		Delay(1); 
		
		P0 = 0x00;		
		P2 = 6;
		P0 = table[angle/10%10];
		Delay(1); 
				
		P0 = 0x00;
		P2 = 5;
		P0 = table[angle/100%10];
		Delay(1);
	}	
}

void KEY(void)
{
	uchar lianan = 0;
	if(KEY1 == 0)	   //按P3.2，实现步进电机的顺时针转动
	{
		Delay_LED(5);
		if(KEY1 == 0)
		{
			FB_flag = 0;
			//FB_flag = 1;
		}
		Delay_LED(5);
	}
	if((KEY2 == 0) && (angle > 0))		//按P3.3，实现步进电机的调速
	{
		Delay(15);
		if((KEY2 == 0) && (angle > 0))
		{
			angle++;
		}
		Delay_LED(100);
		if((KEY2 == 0) && (angle > 0))
		{
			Delay_LED(100);
			angle++;
			if((KEY2 == 0) && (angle > 0))
			{
				Delay_LED(80);
				angle++;
				while(1)
				{
					if((KEY2 == 0) && (angle > 0))
					{
						angle++;
						Delay_LED(20);	
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	if((KEY3 == 0) && (angle < 360))		//按P3.4，实现步进电机的调速
	{
		Delay_LED(15);
		if((KEY3 == 0)&& (angle < 360))
		{
			angle--;
		}
		Delay_LED(100);
		if((KEY3 == 0)&& (angle < 360))
		{
			Delay_LED(100);
			angle--;
			if((KEY3 == 0) && (angle < 360))
			{
				Delay_LED(80);
				angle--;
				while(1)
				{
					if((KEY3 == 0)&& (angle < 360))
					{
						angle--;
						Delay_LED(20);	
					}
					else
					{
						break;
					}
				}
			}
		}
	}
}

main()
{
 	uchar i;
	uint k = 0;
	float temp=0;
	while(1)
	{
		KEY();
		if(KEY1 == 0)
		{
			break;
		}
		Delay_LED(1);
	}	
	while(1)
	{
		temp = angle;
		for(k=0;k<512;k++)
		{
			if(FB_flag == 0)
			{
		  		for(i=0;i<8;i++)      		//因为有8路的控制时序
		   		{
		     		P1 = F_Rotation[i];  	//顺时针转动
		     		Delay_LED(1);       	//改变这个参数可以调整电机转速
				}
			}
			else
			{
				for(i=0;i<8;i++)      		//因为有8路的控制时序
		    	{
		     		P1 = B_Rotation[i];  	//顺时针转动
		     		Delay_LED(1);       	//改变这个参数可以调整电机转速
				}	
			}
			temp = temp - 0.703;
			angle = (uint)temp;
			Delay_LED(1);
			if(angle == 0)
			{
				break;
			}
		}
		if(angle == 0)
		{
			break;
		}
	}
	while(1)
	{
		Delay_LED(1);	
	}
}
