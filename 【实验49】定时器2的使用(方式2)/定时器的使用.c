/********************************************************************
* 描述    :  用定时器方式2的程序代码。
			 相对前面的软件延时，定时器的延时更为精确。
			 晶振为12M，延时时间为50毫秒。
* 创建人  ： 东流，2012年2月8日
* 版本号  ： 2.0
***********************************************************************/
#include<reg52.h>
#define uchar unsigned char
#define uint  unsigned int 

uint Count = 0;
uchar code table[10] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};
/********************************************************************
* 名称 : Time0_Init()
* 功能 : 定时器的初始化，12MZ晶振，0.1ms
* 输入 : 无
* 输出 : 无
***********************************************************************/
void Time0_Init()
{
	RCAP2H = (65536-50000)/256;//晶振12M 50ms 16bit 自动重载
    RCAP2L = (65536-50000)%256;
    ET2=1;                     //打开定时器中断
    EA=1;                      //打开总中断
    TR2=1;                     //打开定时器开关		
}
										 
/********************************************************************
* 名称 : Time0_Int()
* 功能 : 定时器中断，中断中实现 Count 加一
* 输入 : 无
* 输出 : 无
***********************************************************************/
void Time0_Int() interrupt 5 using 1
{
	TF2=0;
	Count++;			//长度加1
}

/********************************************************************
* 名称 : Main()
* 功能 : 主函数
* 输入 : 无
* 输出 : 无
***********************************************************************/
void main()
{
	uchar i = 0;
	Time0_Init();
	P2 = 7;
	while(1)
	{
		P0 = table[i % 10];	  //取 i 的个位
		while(1)
		{
			if(Count == 20)	  //当Count为 10000 时，i 自加一次，20 * 50MS = 1S
			{
				Count = 0;
				i++;
				break;
			}
		}
	}
}